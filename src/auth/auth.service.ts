import { Injectable } from '@nestjs/common';
import { SignUpDto } from './dto/signUp.dto';
import { LoginDto } from './dto/login.dto';
import * as bcrypt from 'bcrypt';
import { DB } from './db/user.db';

@Injectable()
export class AuthService {
  constructor() {}
  async login(loginDto: LoginDto) {
    try {
      /**
       * // TODO: 7. [hands-on] Code Suggestions 의 도움을 받아 아래 로직으로 기능을 개발해주세요.
       * 1. loginDto 에 있는 email 을 기준으로 DB 리스트에서 검색
       * 2. 만약 데이터가 존재 하지 않으면 '유저가 존재하지 않습니다.' 에러 반환
       * 3. DB 에 있는 해시값과 일치하는지 확인
       *    3-1. 만약 일치하지 않으면 '비밀번호가 일치하지 않습니다.' 에러 반환
       *    3-2. 일치하면 '로그인 성공' 반환
       */
    } catch (error) {
      throw error;
    }
  }
  async signup(signupDto: SignUpDto) {
    try {
      /**
       * // TODO: 8. [hands-on] Code Suggestions 의 도움을 받아 아래 로직으로 기능을 개발해주세요.
       * 1. DB 리스트에 동일한 이메일을 가지는 데이터가 있는지 확인
       *    1-1. 만약 존재하면 '동일한 이메일이 존재합니다.' 에러 반환
       * 2. 비밀번호를 해시값으로 변환
       * 3. DB 리스트에 데이터 추가
       * 4. 생성된 데이터를 반환
       */
    } catch (error) {
      throw error;
    }
  }
}
