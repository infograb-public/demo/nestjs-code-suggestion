import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('auth routes', () => {
    // TODO: 9. [hands-on] Code Suggestions 의 도움을 받아서 테스트 코드를 작성하세요.
    const sampleData = {
      email: 'william.henry.harrison@example-pet-store.com',
      password: '123456',
    };
  });
});
