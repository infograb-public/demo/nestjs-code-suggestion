# NestJS GitLab Duo Code Suggestions Hands-on Project

본 프로젝트는 인포그랩 유한회사에서 작성한 GitLab Duo Code Suggestions 핸즈온 프로젝트 입니다. 사용 프레임워크는 NestJS 이므로 최종적으로 실행하기 위해서는 npm 을 설치해야 합니다.

본 프로젝트를 진행하는데 필요한 extension 은 다음과 같습니다.

- VSCode
  - [GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow)
  - [TODO Highlight](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight)
  - [TODO Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)
- IntelliJ
  - [IntelliJ IDE Plugin Marketplace](https://plugins.jetbrains.com/plugin/22325-gitlab-duo)

## 프로젝트 구성

```
├── README.md
├── nest-cli.json
├── package-lock.json
├── package.json
├── src
│   ├── app.module.ts // app module
│   ├── auth
│   │   ├── auth.controller.ts // 로그인, 회원가입 API 컨트롤러 파일
│   │   ├── auth.module.ts // 인증 모듈
│   │   ├── auth.service.ts // 로그인, 회원가입 API 서비스 파일
│   │   ├── db
│   │   │   └── user.db.ts // DB 배열 선언 파일
│   │   └── dto
│   │       ├── login.dto.ts // 로그인 request dto
│   │       └── signUp.dto.ts // 회원가입 request dto
│   └── main.ts
├── test
│   ├── app.e2e-spec.ts
│   └── jest-e2e.json
├── tsconfig.build.json
└── tsconfig.json
```

> 참고사항: 본 프로젝트는 핸즈온 프로젝트임으로 DB 를 사용하지 않고 객체에 데이터를 `push` 하는 방식으로 진행합니다. entity 와 typeorm 을 이용해서 여러분들이 본 프로젝트를 발전시키는 것도 좋은 연습이 될것입니다.

## Mission

여러분들의 미션은 **Code Suggestions의 도움을 받아서 API 개발**을 하는 것입니다. 개발할 API는 다음과 같습니다.

- `POST /auth/signup`: 회원가입 API
- `POST /auth/login`: 로그인 API

## TODO

본 프로젝트에서는 `TODO: 1. [hands-on] ...` 와 같은 형식으로 순서가 정의되어있습니다. 순서대로 작성되어있는 미션을 수행하면 됩니다. 작성된 미션들 리스트는 다음과 같습니다.

```
TODO: 1. [hands-on] 이메일과 비밀번호를 입력 받기 위한 필드를 Code Suggestions의 도움을 받아 작성해보세요.
TODO: 2. [hands-on] 이메일과 비밀번호를 입력 받기 위한 필드를 Code Suggestions 의 도움을 받아서 작성해 보세요.
TODO: 3. [hands-on] Code Suggestions 의 도움을 받아 ValidationPipe 를 추가해보세요.
TODO: 4. [hands-on] Code Suggestions 의 도움을 받아 ValidationPipe의 세부 설정을 추가해보세요.
TODO: 5. [hands-on] Code Suggestions 의 도움을 받아 아웃라인을 작성해보세요.
TODO: 6. [hands-on] Code Suggestions 의 도움을 받아 작성된 SignUpDto, LoginDto 를 사용해보세요.
TODO: 7. [hands-on] Code Suggestions 의 도움을 받아 아래 로직으로 기능을 개발해주세요.
TODO: 8. [hands-on] Code Suggestions 의 도움을 받아 아래 로직으로 기능을 개발해주세요.
TODO: 9. [hands-on] Code Suggestions 의 도움을 받아서 테스트 코드를 작성하세요.
```
